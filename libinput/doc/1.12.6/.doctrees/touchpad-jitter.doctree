��EL      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`9da9118`�h]�h �	reference���)��}�(h�git commit 9da9118�h]�h �Text����git commit 9da9118�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/9da9118�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f81e92b1598>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f81e92b1598>�h]�h�<git commit <function get_git_version_full at 0x7f81e92b1598>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f81e92b1598>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _touchpad_jitter:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��touchpad-jitter�uh0h]h:Kh hhhh8�;/home/whot/code/libinput/build/doc/user/touchpad-jitter.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Touchpad jitter�h]�h�Touchpad jitter�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(h�bTouchpad jitter describes random movement by a few pixels even when the
user's finger is unmoving.�h]�h�dTouchpad jitter describes random movement by a few pixels even when the
user’s finger is unmoving.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(hXZ  libinput has a mechanism called a **hysteresis** to avoid that jitter. When
active, movement with in the **hysteresis margin** is discarded. If the
movement delta is larger than the margin, the movement is passed on as
pointer movement. This is a simplified summary, developers should
read the implementation of the hysteresis in ``src/evdev.c``.�h]�(h�"libinput has a mechanism called a �����}�(h�"libinput has a mechanism called a �h h�hhh8Nh:Nubh �strong���)��}�(h�**hysteresis**�h]�h�
hysteresis�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh�9 to avoid that jitter. When
active, movement with in the �����}�(h�9 to avoid that jitter. When
active, movement with in the �h h�hhh8Nh:Nubh�)��}�(h�**hysteresis margin**�h]�h�hysteresis margin�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh�� is discarded. If the
movement delta is larger than the margin, the movement is passed on as
pointer movement. This is a simplified summary, developers should
read the implementation of the hysteresis in �����}�(h�� is discarded. If the
movement delta is larger than the margin, the movement is passed on as
pointer movement. This is a simplified summary, developers should
read the implementation of the hysteresis in �h h�hhh8Nh:Nubh �literal���)��}�(h�``src/evdev.c``�h]�h�src/evdev.c�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh�.�����}�(h�.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K
h hnhhubh�)��}�(h��libinput uses the kernel ``fuzz`` value to determine the size of the
hysteresis. Users should override this with a udev hwdb entry where the
device itself does not provide the correct value.�h]�(h�libinput uses the kernel �����}�(h�libinput uses the kernel �h h�hhh8Nh:Nubh�)��}�(h�``fuzz``�h]�h�fuzz�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh�� value to determine the size of the
hysteresis. Users should override this with a udev hwdb entry where the
device itself does not provide the correct value.�����}�(h�� value to determine the size of the
hysteresis. Users should override this with a udev hwdb entry where the
device itself does not provide the correct value.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh^)��}�(h�".. _touchpad_jitter_fuzz_override:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�touchpad-jitter-fuzz-override�uh0h]h:Kh hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h�!Overriding the hysteresis margins�h]�h�!Overriding the hysteresis margins�����}�(hj  h j  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j
  hhh8hkh:Kubh�)��}�(hX  libinput provides the debugging tool ``libinput measure fuzz`` to help edit or
test a fuzz value. This tool is interactive and provides a udev hwdb entry
that matches the device. To check if a fuzz is currently present, simply run
without arguments or with the touchpad's device node:�h]�(h�%libinput provides the debugging tool �����}�(h�%libinput provides the debugging tool �h j  hhh8Nh:Nubh�)��}�(h�``libinput measure fuzz``�h]�h�libinput measure fuzz�����}�(hhh j$  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  ubh�� to help edit or
test a fuzz value. This tool is interactive and provides a udev hwdb entry
that matches the device. To check if a fuzz is currently present, simply run
without arguments or with the touchpad’s device node:�����}�(h�� to help edit or
test a fuzz value. This tool is interactive and provides a udev hwdb entry
that matches the device. To check if a fuzz is currently present, simply run
without arguments or with the touchpad's device node:�h j  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j
  hhubh �literal_block���)��}�(h��$ sudo libinput measure fuzz
Using Synaptics TM2668-002: /dev/input/event17
  Checking udev property... not set
  Checking axes... x=16 y=16�h]�h��$ sudo libinput measure fuzz
Using Synaptics TM2668-002: /dev/input/event17
  Checking udev property... not set
  Checking axes... x=16 y=16�����}�(hhh j?  ubah!}�(h#]�h%]�h']�h)]�h+]��	xml:space��preserve�uh0j=  h:K'h j
  hhh8hkubh�)��}�(h�rIn the above output, the axis fuzz is set to 16. To set a specific fuzz, run
with the ``--fuzz=<value>`` argument.�h]�(h�VIn the above output, the axis fuzz is set to 16. To set a specific fuzz, run
with the �����}�(h�VIn the above output, the axis fuzz is set to 16. To set a specific fuzz, run
with the �h jO  hhh8Nh:Nubh�)��}�(h�``--fuzz=<value>``�h]�h�--fuzz=<value>�����}�(hhh jX  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jO  ubh�
 argument.�����}�(h�
 argument.�h jO  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K(h j
  hhubj>  )��}�(h�%$ sudo libinput measure fuzz --fuzz=8�h]�h�%$ sudo libinput measure fuzz --fuzz=8�����}�(hhh jq  ubah!}�(h#]�h%]�h']�h)]�h+]�jM  jN  uh0j=  h:K3h j
  hhh8hkubh�)��}�(h�mThe tool will attempt to construct a hwdb file that matches your touchpad
device. Follow the printed prompts.�h]�h�mThe tool will attempt to construct a hwdb file that matches your touchpad
device. Follow the printed prompts.�����}�(hj�  h j  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K1h j
  hhubh�)��}�(h�qIn the ideal case, the tool will provide you with a file that can be
submitted to the systemd repo for inclusion.�h]�h�qIn the ideal case, the tool will provide you with a file that can be
submitted to the systemd repo for inclusion.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K4h j
  hhubh�)��}�(h��However, hwdb entry creation is difficult to automate and it's likely
that the tools fails in doing so, especially if an existing entry is already
present.�h]�h��However, hwdb entry creation is difficult to automate and it’s likely
that the tools fails in doing so, especially if an existing entry is already
present.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K7h j
  hhubh�)��}�(h��Below is the outline of what a user needs to do to override a device's fuzz
value in case the ``libinput measure fuzz`` tool fails.�h]�(h�`Below is the outline of what a user needs to do to override a device’s fuzz
value in case the �����}�(h�^Below is the outline of what a user needs to do to override a device's fuzz
value in case the �h j�  hhh8Nh:Nubh�)��}�(h�``libinput measure fuzz``�h]�h�libinput measure fuzz�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh� tool fails.�����}�(h� tool fails.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K;h j
  hhubh�)��}�(hX0  Check with ``udevadm info /sys/class/input/eventX`` (replace your device node
number) whether an existing hwdb override exists. If the ``EVDEV_ABS_``
properties are present, the hwdb override exists. Find the file that
contains that entry, most likely in ``/etc/udev/hwdb.d`` or
``/usr/lib/udev/hwdb.d``.�h]�(h�Check with �����}�(h�Check with �h j�  hhh8Nh:Nubh�)��}�(h�(``udevadm info /sys/class/input/eventX``�h]�h�$udevadm info /sys/class/input/eventX�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�T (replace your device node
number) whether an existing hwdb override exists. If the �����}�(h�T (replace your device node
number) whether an existing hwdb override exists. If the �h j�  hhh8Nh:Nubh�)��}�(h�``EVDEV_ABS_``�h]�h�
EVDEV_ABS_�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�j
properties are present, the hwdb override exists. Find the file that
contains that entry, most likely in �����}�(h�j
properties are present, the hwdb override exists. Find the file that
contains that entry, most likely in �h j�  hhh8Nh:Nubh�)��}�(h�``/etc/udev/hwdb.d``�h]�h�/etc/udev/hwdb.d�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh� or
�����}�(h� or
�h j�  hhh8Nh:Nubh�)��}�(h�``/usr/lib/udev/hwdb.d``�h]�h�/usr/lib/udev/hwdb.d�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�.�����}�(hh�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K>h j
  hhubh�)��}�(hX   The content of the property is a set of values in the format
``EVDEV_ABS_00=min:max:resolution:fuzz``. You need to set the ``fuzz`` part,
leaving the remainder of the property as-is. Values may be empty, e.g. a
property that only sets resolution and fuzz reads as ``EVDEV_ABS_00=::32:8``.�h]�(h�=The content of the property is a set of values in the format
�����}�(h�=The content of the property is a set of values in the format
�h j%  hhh8Nh:Nubh�)��}�(h�(``EVDEV_ABS_00=min:max:resolution:fuzz``�h]�h�$EVDEV_ABS_00=min:max:resolution:fuzz�����}�(hhh j.  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j%  ubh�. You need to set the �����}�(h�. You need to set the �h j%  hhh8Nh:Nubh�)��}�(h�``fuzz``�h]�h�fuzz�����}�(hhh jA  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j%  ubh�� part,
leaving the remainder of the property as-is. Values may be empty, e.g. a
property that only sets resolution and fuzz reads as �����}�(h�� part,
leaving the remainder of the property as-is. Values may be empty, e.g. a
property that only sets resolution and fuzz reads as �h j%  hhh8Nh:Nubh�)��}�(h�``EVDEV_ABS_00=::32:8``�h]�h�EVDEV_ABS_00=::32:8�����}�(hhh jT  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j%  ubh�.�����}�(hh�h j%  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KDh j
  hhubh�)��}�(h�LIf no properties exist, your hwdb.entry should look approximately like this:�h]�h�LIf no properties exist, your hwdb.entry should look approximately like this:�����}�(hjn  h jl  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KIh j
  hhubj>  )��}�(h��evdev:name:Synaptics TM2668-002:dmi:*:svnLENOVO*:pvrThinkPadT440s*:
 EVDEV_ABS_00=:::8
 EVDEV_ABS_01=:::8
 EVDEV_ABS_35=:::8
 EVDEV_ABS_36=:::8�h]�h��evdev:name:Synaptics TM2668-002:dmi:*:svnLENOVO*:pvrThinkPadT440s*:
 EVDEV_ABS_00=:::8
 EVDEV_ABS_01=:::8
 EVDEV_ABS_35=:::8
 EVDEV_ABS_36=:::8�����}�(hhh jz  ubah!}�(h#]�h%]�h']�h)]�h+]�jM  jN  uh0j=  h:KRh j
  hhh8hkubh�)��}�(h��Substitute the ``name`` field with the device name (see the output of
``libinput measure fuzz`` and the DMI match content with your hardware. See
:ref:`hwdb_modifying` for details.�h]�(h�Substitute the �����}�(h�Substitute the �h j�  hhh8Nh:Nubh�)��}�(h�``name``�h]�h�name�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�/ field with the device name (see the output of
�����}�(h�/ field with the device name (see the output of
�h j�  hhh8Nh:Nubh�)��}�(h�``libinput measure fuzz``�h]�h�libinput measure fuzz�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�3 and the DMI match content with your hardware. See
�����}�(h�3 and the DMI match content with your hardware. See
�h j�  hhh8Nh:Nub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`hwdb_modifying`�h]�h �inline���)��}�(hj�  h]�h�hwdb_modifying�����}�(hhh j�  ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0j�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit���	reftarget��hwdb_modifying��refdoc��touchpad-jitter��refwarn��uh0j�  h8hkh:KTh j�  ubh� for details.�����}�(h� for details.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KTh j
  hhubh�)��}�(h��Once the hwdb entry has been modified, added, or created,
:ref:`reload the hwdb <hwdb_reloading>`. Once reloaded, :ref:`libinput-record`
"libinput record" should show the new fuzz value for the axes.�h]�(h�:Once the hwdb entry has been modified, added, or created,
�����}�(h�:Once the hwdb entry has been modified, added, or created,
�h j�  hhh8Nh:Nubj�  )��}�(h�':ref:`reload the hwdb <hwdb_reloading>`�h]�j�  )��}�(hj�  h]�h�reload the hwdb�����}�(hhh j�  ubah!}�(h#]�h%]�(j�  �std��std-ref�eh']�h)]�h+]�uh0j�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j  �refexplicit��j�  �hwdb_reloading�j�  j�  j�  �uh0j�  h8hkh:KXh j�  ubh�. Once reloaded, �����}�(h�. Once reloaded, �h j�  hhh8Nh:Nubj�  )��}�(h�:ref:`libinput-record`�h]�j�  )��}�(hj  h]�h�libinput-record�����}�(hhh j  ubah!}�(h#]�h%]�(j�  �std��std-ref�eh']�h)]�h+]�uh0j�  h j  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j$  �refexplicit��j�  �libinput-record�j�  j�  j�  �uh0j�  h8hkh:KXh j�  ubh�C
“libinput record” should show the new fuzz value for the axes.�����}�(h�?
"libinput record" should show the new fuzz value for the axes.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KXh j
  hhubh�)��}�(h�ERestart the host and libinput should pick up the revised fuzz values.�h]�h�ERestart the host and libinput should pick up the revised fuzz values.�����}�(hjA  h j?  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K\h j
  hhubh^)��}�(h�.. _kernel_fuzz:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�kernel-fuzz�uh0h]h:Kch j
  hhh8hkubeh!}�(h#]�(�!overriding-the-hysteresis-margins�j	  eh%]�h']�(�!overriding the hysteresis margins��touchpad_jitter_fuzz_override�eh)]�h+]�uh0hlh hnhhh8hkh:K�expect_referenced_by_name�}�j^  h�s�expect_referenced_by_id�}�j	  h�subhm)��}�(hhh]�(hr)��}�(h�Kernel fuzz�h]�h�Kernel fuzz�����}�(hjj  h jh  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh je  hhh8hkh:Kbubh�)��}�(hX�  A fuzz set on an absolute axis in the kernel causes the kernel to apply
hysteresis-like behavior to the axis. Unfortunately, this behavior leads to
inconsistent deltas. To avoid this, libinput sets the kernel fuzz on the
device to 0 to disable this kernel behavior but remembers what the fuzz was
on startup. The fuzz is stored in the ``LIBINPUT_FUZZ_XX`` udev property, on
startup libinput will check that property as well as the axis itself.�h]�(hXO  A fuzz set on an absolute axis in the kernel causes the kernel to apply
hysteresis-like behavior to the axis. Unfortunately, this behavior leads to
inconsistent deltas. To avoid this, libinput sets the kernel fuzz on the
device to 0 to disable this kernel behavior but remembers what the fuzz was
on startup. The fuzz is stored in the �����}�(hXO  A fuzz set on an absolute axis in the kernel causes the kernel to apply
hysteresis-like behavior to the axis. Unfortunately, this behavior leads to
inconsistent deltas. To avoid this, libinput sets the kernel fuzz on the
device to 0 to disable this kernel behavior but remembers what the fuzz was
on startup. The fuzz is stored in the �h jv  hhh8Nh:Nubh�)��}�(h�``LIBINPUT_FUZZ_XX``�h]�h�LIBINPUT_FUZZ_XX�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jv  ubh�X udev property, on
startup libinput will check that property as well as the axis itself.�����}�(h�X udev property, on
startup libinput will check that property as well as the axis itself.�h jv  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kdh je  hhubeh!}�(h#]�(jW  �id2�eh%]�h']�(�kernel fuzz��kernel_fuzz�eh)]�h+]�uh0hlh hnhhh8hkh:Kbja  }�j�  jM  sjc  }�jW  jM  subeh!}�(h#]�(hj�id1�eh%]�h']�(�touchpad jitter��touchpad_jitter�eh)]�h+]�uh0hlh hhhh8hkh:Kja  }�j�  h_sjc  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_aj	  ]�h�ajW  ]�jM  au�nameids�}�(j�  hjj�  j�  j^  j	  j]  jZ  j�  jW  j�  j�  u�	nametypes�}�(j�  �j�  Nj^  �j]  Nj�  �j�  Nuh#}�(hjhnj�  hnj	  j
  jZ  j
  jW  je  j�  je  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�h�)��}�(hhh]�h�5Hyperlink target "touchpad-jitter" is not referenced.�����}�(hhh j5  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j2  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type��INFO��source�hk�line�Kuh0j0  ubj1  )��}�(hhh]�h�)��}�(hhh]�h�CHyperlink target "touchpad-jitter-fuzz-override" is not referenced.�����}�(hhh jP  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jM  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�jJ  �source�hk�line�Kuh0j0  ubj1  )��}�(hhh]�h�)��}�(hhh]�h�1Hyperlink target "kernel-fuzz" is not referenced.�����}�(hhh jj  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jg  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�jJ  �source�hk�line�Kcuh0j0  ube�transformer�N�
decoration�Nhhub.